# Conditional StyleGAN modelling and analysis for a machining digital twin

A repository for the open access files related to the publication "Conditional StyleGAN modelling and analysis for a machining digital twin" by Evgeny Zotov, Ashutosh Tiwari and Visakan Kadirkamanathan from the University of Sheffield.